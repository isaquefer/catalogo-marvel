import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ComicService } from '../../shared/services/comic.service';
import { Comic } from '../../shared/interfaces/comic';

@Component({
    selector: 'app-catalogo',
    templateUrl: './catalogo.component.html',
    styleUrls: ['./catalogo.component.less']
})
export class CatalogoComponent implements OnInit {

    public comic: Comic[];
    public href: String;

    constructor(
        private router: Router,
        private comicService: ComicService,
    ) { }

    getComic() {
        this.comicService.getComic().subscribe((comic: any) => {
            this.comic = comic.data.results.filter((comic: Comic) => {
                return comic.description != null && comic.title != null;
            });
        });
    }

    ngOnInit() {
        this.href = this.router.url;
        this.getComic();
    }
}
