import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { baseUrl } from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})

export class ComicService {

    constructor(
        private http: HttpClient,
    ) { }

    getComic() {
        return this.http.get(baseUrl.getComicUrl).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong.
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
            // Return an observable with a user-facing error message.
        return throwError('Something bad happened; please try again later.');
    }
}
