import { Component, OnInit } from '@angular/core';

import { NAVMENU } from '../../../shared/utils/menu';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

    public navMenu = NAVMENU;
    public href: String;

    constructor() { }

    ngOnInit() {
    }

    onActivate(componentReference: any) {
        this.href = componentReference.router.url;
    }
}
