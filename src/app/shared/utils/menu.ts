import { NavMenu } from '../interfaces/nav-menu';


export const NAVMENU: NavMenu[] = [
    {title: 'Home', href: '/'},
    {title: 'Catálogo', href: '/catalogo'},
    {title: 'Right Sidebar', href: 'right-sidebar.html'},
    {title: 'No sidebar', href: 'no-sidebar.html'},
    {title: 'Dropdown', href: 'no-sidebar.html'},
];
