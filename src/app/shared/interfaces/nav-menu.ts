export interface NavMenu {
    title: String;
    href: String;
}
