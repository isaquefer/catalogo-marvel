// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

let apiUrl = 'https://gateway.marvel.com:443/v1/public/';
let ts = '?ts=1';
let apiKey = '&apikey=d1d0cfaff8621c8db2cd083087ab95c8';
let hash = '&hash=1f9fa43a100d8685b34f3370189bab54';
let key = ts + apiKey + hash;

export const baseUrl =  {
  getComicUrl: apiUrl + 'comics' + key
}




/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
